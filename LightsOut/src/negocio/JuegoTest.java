package negocio;



import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class JuegoTest {

	@Test
	public void testLucesvecinas()
	{
		Juego juego = new Juego(3);
		int[] esperado = {1,3,5,7,4};
		Assert.iguales(esperado, juego.getVecinosDeLuz(4));
	}
	
	@Test
	public void testLucesvecinasEnExtremo()
	{
		Juego juego = new Juego(3);
		int[] esperado = {5,7,8};
		Assert.iguales(esperado, juego.getVecinosDeLuz(8));
	}
	
	@Test
	public void testJugada()
	{
		Juego juego = new Juego(3);
		boolean[] antesDeJugar =  juego.getEstadoPartidaPorLuz(0);
		juego.invertirEstadoLuz(0);
		boolean[] DespuesDeJugar =  juego.getEstadoPartidaPorLuz(0);
		
		for (int i = 0; i < DespuesDeJugar.length; i++)
		{
			assertEquals(!antesDeJugar[i],DespuesDeJugar[i]);
		}
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testInvertirEstadoLuz()
	{
		Juego juego = new Juego(3); // jeugo d 3x3 = 9 (limite 9)
		juego.invertirEstadoLuz(9);
	}
	
	

}
