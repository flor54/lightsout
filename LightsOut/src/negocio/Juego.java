package negocio;

import java.util.Random;
import java.util.Set;

public class Juego
{
	private boolean[] luces;
	private Grafo lucesVecinas;
	private int lucesPorFila;
	private int lucesEnTotal;
	
	public Juego(int tamaņoDeFila)
	{
		this.limitarFilas(tamaņoDeFila);
		this.lucesPorFila = tamaņoDeFila;
		this.lucesEnTotal = (int) Math.pow(this.lucesPorFila, 2);
		this.luces = new boolean[this.lucesEnTotal];
		this.lucesVecinas = new Grafo(this.lucesEnTotal);
		this.calcularLucesVecinas();
		this.prenderLucesAlAzar();
	}
	
	private void limitarFilas(int filas) 
	{
		if(filas < 3 || filas > 6) 
		{
			throw new IllegalArgumentException("La cantidad de filas por columna debe ser menor a 6 y mayor a 3");
		}
	}
	
	public boolean[] getEstadoPartidaPorLuz(int luz)
	{
		this.verificarLuzValida(luz);
		Set<Integer> vecinos = this.getVecinosDeLuz(luz);
		boolean[] ret = new boolean[vecinos.size()];
		int i = 0;
		for(Integer x : vecinos) 
		{
			ret[i] = this.getEstadoLuz(x);
			i++;
		}
		return ret;
	}
	
	public boolean getEstadoLuz(int luz)
	{
		this.verificarLuzValida(luz);
		return this.luces[luz];
	}
	
	public boolean[] getLuces() 
	{
		return this.luces;
	}

	private void prenderLucesAlAzar()
	{
		Random random = new Random();
		for(int i = 0;i < this.lucesEnTotal;i++)
		{
			this.luces[i] = random.nextBoolean();
		}
		this.verificarHayLuzPrendida();
	}

	private void verificarHayLuzPrendida()
	{	
		boolean hayLuzEncendida = false;
		int luz = 0;
		while(!hayLuzEncendida)
		{
			hayLuzEncendida = hayLuzEncendida || this.luces[luz]; 
			luz++;
		}
		if(!hayLuzEncendida)
		{
			invertirEstadoLuz(this.lucesEnTotal-1);
		}
	}
	
	public void invertirEstadoLuz(int luz)
	{
		verificarLuzValida(luz);
		Set<Integer> vecinos = this.getVecinosDeLuz(luz);
		for(Integer x : vecinos)
		{
			boolean estadoInverso = !this.luces[x];
			this.luces[x] = estadoInverso;
		}	
	}

	private void verificarLuzValida(int luz)
	{
		if(luz < 0 || luz >= this.lucesEnTotal)
			throw new IllegalArgumentException("No existe la luz ingresada");
	}
	
	@Override
	public String toString()
	{
		String ret = "";
		for (int i = 0; i < this.lucesEnTotal; i+=3) 
		{
			for (int j = i; j < i+3; j++)
			{
				if(this.luces[j])
				{
					ret = ret + "1" ;
				}else
				{
					ret = ret + "0" ;
				}
			}
			ret  = ret + "\n";
		}
		return ret;
	}

	public Set<Integer> getVecinosDeLuz(int luz)
	{
		this.verificarLuzValida(luz);
		Set<Integer> luces  = this.lucesVecinas.vecinos(luz);
		luces.add(luz);
		return luces;
	}
	
	private void calcularLucesVecinas()
	{
		for(int luz = 0; luz < this.lucesEnTotal; luz++)
		{
			agregarLuzAbajo(luz);
			agregarLuzDerecha(luz);
		}
	}

	private void agregarLuzAbajo(int luz)
	{
		int luzAbajo = luz+this.lucesPorFila;
		if(luzAbajo<this.lucesEnTotal)
		{
			relacionarLucesVecinas(luz, luzAbajo);
		}
	}

	private void agregarLuzDerecha(int luz)
	{
		int luzDerecha = luz+1;
		if(luzDerecha%this.lucesPorFila!=0)
		{
			relacionarLucesVecinas(luz, luzDerecha);
		}
	}

	private void relacionarLucesVecinas(int luz, int luzVecina)
	{
		this.lucesVecinas.agregarArista(luz, luzVecina);
	}
	
	public int getCantidadLucesPorFila()
	{
		return this.lucesPorFila;
	}
	
	public int getCantidadLucesEnTotal()
	{
		return this.lucesEnTotal;
	}
	
	public boolean partidaGanada() 
	{
		int  prendidas = 0;
		boolean ret = false ;
		for (int i = 0; i < luces.length; i++)
		{
			if(this.luces[i])
				prendidas++;
		}
		if(prendidas == 0)
			ret = true;
		return ret;
	}
}
