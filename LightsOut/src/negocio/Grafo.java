package negocio;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo
{
	// Representamos un grafo por listas de vecinos
	private ArrayList<Set<Integer>> _vecinos;

	// La cantidad de v�rtices queda fija desde la construcci�n
	public Grafo(int vertices)
	{
		// El grafo comienza con todos los v�rtices aislados
		_vecinos = new ArrayList<Set<Integer>>(vertices);
		
		for(int i=0; i<vertices; ++i)
			_vecinos.add(new HashSet<Integer>());
	}
	
	// Cantidad de v�rtices
	public int vertices()
	{
		return _vecinos.size();
	}
	
	// Agregar una arista
	public void agregarArista(int i, int j)
	{
		verificarIndicesValidos(i, j);
		verificarAristaInexistente(i, j);
		
		_vecinos.get(i).add(j);
		_vecinos.get(j).add(i);
	}

	// Elimina una arista
	public void eliminarArista(int i, int j)
	{
		verificarIndicesValidos(i, j);
		verificarAristaExistente(i, j);
		
		_vecinos.get(i).remove(j);
		_vecinos.get(j).remove(i);
	}
	
	// Consulta si existe una arista
	public boolean existeArista(int i, int j)
	{
		verificarIndicesValidos(i, j);
		return _vecinos.get(i).contains(j);
	}
	
	// Vecinos de un v�rtice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		return _vecinos.get(i);
	}	
	
	// Lanza excepciones si i no es un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 || i >= vertices() )
			throw new IllegalArgumentException("El vertice " + i + " no existe!");
	}
	// Lanza excepciones si (i,j) no puede ser una arista nueva
	private void verificarIndicesValidos(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		
		if( i == j )
			throw new IllegalArgumentException("No se pueden agregar loops! V�rtice = " + i);
	}
	
	// Lanza una excepci�n si la arista existe
	private void verificarAristaInexistente(int i, int j)
	{
		if( existeArista(i, j) == true )
			throw new IllegalArgumentException("Se intent� agregar una arista existente! (i,j) = (" + i + "," + j +")");
	}
	
	// Lanza una excepci�n si la arista no existe
	private void verificarAristaExistente(int i, int j)
	{
		if( existeArista(i, j) == false )
			throw new IllegalArgumentException("Se intent� eliminar una arista inexistente! (i,j) = (" + i + "," + j +")");
	}
}




