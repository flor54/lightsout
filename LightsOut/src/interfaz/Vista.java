package interfaz;

import java.awt.Color;


import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;


import java.util.ArrayList;

public class Vista extends JFrame
{
	private static final long serialVersionUID = 1L;
	private ArrayList<JButton> botones;
	private int botonesPorFila;
	private int DIMENSIONESVENTANA;

	public Vista(int n)
	{
		super();
		this.setVisible(true);
		this.DIMENSIONESVENTANA = 600;
		limitarFilas(n);
		initialize(n);
	}
	
	public ArrayList<JButton> getBotones()
	{
		return this.botones;
	}

	public void setBotones(ArrayList<JButton> botones)
	{
		this.botones = botones;
	}
	
	public void setearColoresPredeterminados(boolean[] luces)
	{
		for (int i = 0; i < luces.length; i++)
		{
			JButton btn = this.botones.get(i);
			if(luces[i]) 
			{
				btn.setBackground(Color.yellow);
			}
			else
			{
				btn.setBackground(Color.blue);
			}
		}
	}
	
	public void cambiarColor(int luz)
	{	
		JButton btn = this.botones.get(luz);
		Color color = btn.getBackground();
		
		if(color.equals(Color.blue))
			btn.setBackground(Color.yellow);
		if(color.equals(Color.yellow))
			btn.setBackground(Color.blue);
	}
	
	private void initialize(int botonesPorFila)
	{
		this.botonesPorFila = botonesPorFila;
		int dimensionesBoton = (int) DIMENSIONESVENTANA / this.botonesPorFila;
		this.botones = new ArrayList<JButton>();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		this.setBounds(100,100,this.DIMENSIONESVENTANA,this.DIMENSIONESVENTANA+50);
		
		for(int botonEnColumna = 0; botonEnColumna < this.botonesPorFila; botonEnColumna++)
		{
			int altoBoton = dimensionesBoton*botonEnColumna;
			
			for(int botonEnFila = 0; botonEnFila < this.botonesPorFila; botonEnFila++)
			{
				int anchoBoton = dimensionesBoton*botonEnFila;
				JButton boton = new JButton("");
				
				boton.setBounds(altoBoton,anchoBoton, dimensionesBoton, dimensionesBoton);
				this.botones.add(boton);
				this.getContentPane().add(boton);
			}
		}
	}
	
	private void limitarFilas(int filas) 
	{
		if(filas < 3 || filas > 6) 
			throw new IllegalArgumentException("La cantidad de filas por columna debe ser menor a 6 y mayor a 3");
	}

	//@SuppressWarnings("deprecation")
	public int partidaGanada()
	{
		int ret = 0;
		Integer[] posiblesValores = { 3,4, 5 ,6};
		Object eleccion =  JOptionPane.showInputDialog(null,
		"Seleccione siguiente Nivel", "¡Ganaste!",
		JOptionPane.INFORMATION_MESSAGE, null,
		posiblesValores, posiblesValores[0]);
		if (eleccion == null )
		{
			System.exit(0);
		}
		else {
			ret = (int) eleccion;
		}
		return ret;
	}
	
	public int getBotonesPorFila() 
	{
		return this.botonesPorFila;
	}
}
