package main;

import controlador.Controller;
import interfaz.Vista;
import negocio.Juego;

public class Main
{
	public static void main(String[] args)
	{
		Juego juego = new Juego(3);
		Vista vista = new Vista(3);
		@SuppressWarnings("unused")
		Controller controller = new Controller(juego,vista);
	}
}
