package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.JButton;
import interfaz.Vista;
import negocio.Juego;

public class Controller implements ActionListener{
	private Juego juego;
	private Vista vista;
	private ArrayList<JButton> btns;
	
	public Controller(Juego juego, Vista vista)
	{
		this.juego = juego;
		this.vista = vista;
		this.inicializar();
	}
	
	private void inicializar()
	{
		btns = this.vista.getBotones();
		
		for (int i = 0; i < this.btns.size(); i++) 
		{
			btns.get(i).addActionListener(this);
		}
		this.vista.setearColoresPredeterminados(juego.getLuces());
	}
	
	public void actionPerformed(ActionEvent e)
	{
		boolean partidaGanada = juego.partidaGanada();
		if(!partidaGanada)
		{
			for (int i = 0; i < this.btns.size(); i++) 
			{
				if(e.getSource().equals(this.btns.get(i)))			
				{
					juego.invertirEstadoLuz(i);
					Set<Integer> vecinosActalizar = juego.getVecinosDeLuz(i);
					this.actualizarVistaConJugada(vecinosActalizar);
					partidaGanada = juego.partidaGanada();
				}
			}
		}
		if(partidaGanada) 
		{
			int nuevaPartida = vista.partidaGanada();
			Vista anterior = this.vista;
			
			anterior.dispose();
			this.juego = new Juego(nuevaPartida);
			this.vista = new Vista(nuevaPartida);
			this.inicializar();
		}
	}
	
	public void actualizarVistaConJugada(Set<Integer> lucesVecinas)
	{
		for(Integer luz : lucesVecinas)
		{
			vista.cambiarColor(luz);
		}
	}
}
